# Smarte License

The Smarte License server. A multi-licensee server used to issue revocable, 
time-limited keys to any number of HTTP clients.

Smarte License works by associating keys (for example a hardware ID)
to user accounts registered on this site. It allows for multiple licenees and licensors.
                  
A client is an application that can be issued a key.
The client can then use the verification endpoint to check the validity of the key.
Note the client is also required to send the user's email, to prevent verification against
other users' keys.
                   
To support multiple licensees, any user on this server can issue and track their own private
collection of keys.

# Development

Run `dotnet run` in root directory.

# Deployment

This app is dockerized. Run `docker-compose up --build` to run a production build ready to accept HTTPS connections.
