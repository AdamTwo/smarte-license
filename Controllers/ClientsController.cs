﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SmarteLicense.Data;
using SmarteLicense.Models;

namespace SmarteLicense.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ClientsController : ControllerBase
    {
        private readonly ApplicationDbContext _context;
        private readonly UserManager<IdentityUser> _userManager;

        public ClientsController(ApplicationDbContext context, UserManager<IdentityUser> userManager)
        {
            _context = context;
            _userManager = userManager;
        }

        // GET: api/Clients
        // This endpoint is used to add clients through a get request for simplicity
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Client>>> AddClient(string name, string email, string password, int maxConnections, string key, int days)
        {
            if (name == null || email == null || password == null || key == null) return BadRequest();

            IdentityUser user = await _userManager.FindByEmailAsync(email);

            bool is_password_valid = await _userManager.CheckPasswordAsync(user, password);

            if (!is_password_valid) return Unauthorized();

            _context.Client.Add(new Client()
            {
                Name = name,
                UserId = user.Id,
                MaxConnections = maxConnections,
                Key = key,
                ExpiryDate = DateTime.Now.AddDays(days)
            });
            await _context.SaveChangesAsync();
            return Ok();
        }
    }
}
