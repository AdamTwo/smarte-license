﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Identity;

namespace SmarteLicense.Models
{
    public class Client
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string Key { get; set; }

        [DataType(DataType.Date)]
        public DateTime ExpiryDate { get; set; }
        public bool Revoked { get; set; }

        public string UserId { get; set; }
        public IdentityUser User { get; set; }

        public int MaxConnections { get; set; }

    }
}
