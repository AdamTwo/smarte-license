﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using SmarteLicense.Data;
using SmarteLicense.Models;

namespace SmarteLicense
{
    public class VerifyModel : PageModel
    {
        private readonly SmarteLicense.Data.ApplicationDbContext _context;

        public VerifyModel(SmarteLicense.Data.ApplicationDbContext context)
        {
            _context = context;
        }

        public Client Client { get; set; }

        public async Task<IActionResult> OnGetAsync(string key, string email, bool? api)
        {
            if (key == null || email == null)
            {
                return NotFound();
            }

            Client = await _context.Client
                .FirstOrDefaultAsync(c => 
                    c.User.Email == email 
                    && c.Key == key
                    && c.ExpiryDate > DateTime.Now
                    && !c.Revoked);

            if (Client == null)
            {
                return Unauthorized();
            }
            else
            {
                Client.User = new IdentityUser { 
                    Email = email
                };
            }
            if (api == true)
            {
                return StatusCode(200, Client.MaxConnections);
            }
            else
            {
                return Page();
            }
        }
    }
}
