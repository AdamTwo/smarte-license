#See https://aka.ms/containerfastmode to understand how Visual Studio uses this Dockerfile to build your images for faster debugging.

FROM mcr.microsoft.com/dotnet/core/aspnet:3.1-buster-slim AS base
WORKDIR /app

FROM mcr.microsoft.com/dotnet/core/sdk:3.1-buster AS build
WORKDIR /src
COPY ["SmarteLicense.csproj", "./"]
RUN dotnet restore "SmarteLicense.csproj"
COPY . .
WORKDIR "/src/"
RUN dotnet build "SmarteLicense.csproj" -c Release -o /app/build

FROM build AS publish
RUN dotnet publish "SmarteLicense.csproj" -c Release -o /app/publish

FROM publish AS migrate
RUN dotnet tool install -g dotnet-ef --version 3.1

FROM migrate AS final
WORKDIR /app
COPY --from=publish /app/publish .
COPY ./entrypoint.sh .
RUN sed -i.bak 's/\r$//' ./entrypoint.sh
RUN chmod +x ./entrypoint.sh
CMD /bin/bash ./entrypoint.sh
