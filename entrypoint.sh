#!/bin/bash

set -e
run_cmd="dotnet SmarteLicense.dll"


until /root/.dotnet/tools/dotnet-ef database update --project /src;  do
>&2 echo "Database is starting up..."
sleep 1
done

>&2 echo "Ready - starting kestrel"
exec $run_cmd